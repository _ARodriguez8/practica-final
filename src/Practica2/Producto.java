package Practica2;

public class Producto {
	public int Codigo;
	public String Descripcion;
	public String Familia;
	public double Precio;
	public byte marcaValido;
	public Producto(int Codigo, String Descripcion, String Familia, double Precio, byte marcaValido) {
		this.Codigo=Codigo;
		this.Descripcion=Descripcion;
		this.Familia=Familia;
		this.Precio=Precio;
		this.marcaValido=marcaValido;
	}
	public int getCodigo() {
		return Codigo;
	}
	public void setCodigo(int codigo) {
		Codigo = codigo;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getFamilia() {
		return Familia;
	}
	public void setFamilia(String familia) {
		Familia = familia;
	}
	public double getPrecio() {
		return Precio;
	}
	public void setPrecio(double precio) {
		Precio = precio;
	}
	public byte getMarcaValido() {
		return marcaValido;
	}
	public void setMarcaValido(byte marcaValido) {
		this.marcaValido = marcaValido;
	}
}
