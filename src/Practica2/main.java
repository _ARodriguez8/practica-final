package Practica2;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class main {
	//tama�o de los campos del registro binario
	static final int tam_codigo=4;
	static final int tam_descripcion=200;
	static final int tam_familia=16;
	static final int tam_precio=8;
	static final int tam_mValido=1;
	static final int tam_registro=tam_codigo+tam_descripcion+tam_familia+tam_precio+tam_mValido;
	static final int numproductos=7137;
	static final int tamproducto=229;
	public static void main(String[] args) {
		try {
		// Declaro RandomAccessFile para acceder a archivo
		RandomAccessFile raf= new RandomAccessFile("Productos.dat","rw");
		//declaramos variable en la que guardamos  el byte leido
		Byte valido;
		//declaramos int para guardar codigo
		int codigo;
		//declaro string para guardar la descripcion
		String descripcion="";
		//declaro string familia para guardar familia
		String familia="";
		//declaro double en el que guardar precio
		double precio;
		//bucle que recorre el numero de productos existentes obteniendo numero de bytes del archivo y dividiendolo
		//entre numero de bytes de un producto
		for(int i=1;i<raf.length()/229;i++) {
			//posicionamos puntero en posicion dnde se encuentra marca de valido
			raf.seek((229*i)-1);
			
			valido=raf.readByte();
			if(valido==2) {
				System.out.println("Este es el "+i);
				//situa el puntero en la posicion inicial de la linea
				raf.seek(raf.getFilePointer()-229);
				
				codigo=raf.readInt();
				
				//bucle para recorrer los 100 caracteres de la descripcion
				for(int j=0;j<100;j++) {
					//guardo en descripcion cada char leido hasta comletar la descripcion
					descripcion+=raf.readChar();
				}
				
				//bucle para recorrer los 8 bytes que tiene ocupa la familia
				for(int a=0;a<8;a++) {
					//guardo en familia cada char hasta completarla
					familia+=raf.readChar();
				}
				
				precio=raf.readDouble();
				
				System.out.println(codigo+" ");
				System.out.println(descripcion+" ");
				System.out.println(familia+" ");
				System.out.println(precio+" ");
				System.out.println(valido+" ");
			}
			
		}
		raf.close();
		}catch(IOException e) {
			System.out.println(e);
		}
	}

}
