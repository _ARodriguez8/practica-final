package Practica1;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class config {
	public static void main(String [] args) {
		Properties configuracion=new Properties();
		//indicamos el nombre del fichero usado para generar el nombre de las mujeres
		configuracion.setProperty("fnombremujer", "C:\\Users\\AlumnoManana20-21\\Downloads\\mujeres.txt");
		//indicamos el nombre del fichero usado para generar el nombre de los hombres
		configuracion.setProperty("fnombrehombre", "C:\\Users\\AlumnoManana20-21\\Downloads\\hombres.txt");
		//indicamos el nombre del fichero usado para generar los apellidos
		configuracion.setProperty("fapellidos", "C:\\Users\\AlumnoManana20-21\\Downloads\\apellidos.txt");
		//establecemos el numero de personas
		configuracion.setProperty("numPersonas", "2000");
		//establecemos la edad minima
		configuracion.setProperty("minEdad", "0");
		//establecemos la edad maxima
		configuracion.setProperty("maxEdad", "110");
		//% estimado de hombres
		configuracion.setProperty("%hombre", "55");
		//% estimado de mujeres
		configuracion.setProperty("%mujer", "45");
		try {
			configuracion.store(new FileOutputStream("C:\\Users\\AlumnoManana20-21\\Desktop\\PracticaProg\\src\\Practica1\\Practica1.cfg"),"Fichero de configuracion");
			System.out.println("Fichero que contiene los nombres de mujeres: " +configuracion.getProperty("fnombremujer")+"\n"+
								"Fichero que contiene los nombres de hombres: " +configuracion.getProperty("fnombrehombre")+"\n"+
								"Fichero que contiene los apellidos: " +configuracion.getProperty("fapellidos")+"\n"+
								"Numero de personas en el fichero: " +configuracion.getProperty("numPersonas")+"\n"+
								"Edad minima de los participantes: " +configuracion.getProperty("minEdad")+"\n"+
								"Edad maxima de los participantes: " +configuracion.getProperty("maxEdad")+"\n"+
								"Porcentaje de hombres en el fichero: " +configuracion.getProperty("%hombre")+"\n"+
								"Porcentaje de mujeres en el fichero: " +configuracion.getProperty("%mujer"));
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

}
